from flask import Flask
from flask import render_template
from flask import url_for
import requests

app = Flask(__name__)

@app.route("sofkatest.netlify.app")
def home():
    img1 = url_for('static', filename='assets/img/2.png')
    img2 = url_for('static', filename='assets/img/3.png')
    data = dataNutrition()
    return render_template('index.html', im1=img1, im2=img2, info=data)

def dataNutrition():
    info = [0]*4
    url = "https://edamam-edamam-nutrition-analysis.p.rapidapi.com/api/nutrition-data"
    querystring = {"ingr":"1 large apple"}
    headers = {
        'x-rapidapi-host': "edamam-edamam-nutrition-analysis.p.rapidapi.com",
        'x-rapidapi-key': "8fd3197044msh9c46720a6286a2fp1fe5c5jsne24e1fcee1db"
        }
    response = requests.request("GET", url, headers=headers, params=querystring)
    data = response.json()
    i = 0
    for dat in data['totalNutrients']:
        if(i <= 3):
            info[i] = data['totalNutrients'][dat]
        i += 1
    return info